var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var moment = require("moment");

const session = require('express-session');
const mongoDbStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const bodyParser = require('body-parser');
const flash = require('connect-flash');
const swal = require('sweetalert');

const MONOGO_DB_URI = 'mongodb://ivana:nopass5@ds131765.mlab.com:31765/bring-home';
var authRouter = require('./routes/auth');
var listRouter = require('./routes/list');
var chatRouter = require('./routes/chat');
const User = require('./models/user');

var app = express();
const store = new mongoDbStore({
  uri: MONOGO_DB_URI,
  collection: 'sessions'
});
app.use(bodyParser.urlencoded({
  extended: false
}));
const csrfProtection = csrf();
app.locals.moment = require("moment");  
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(flash());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({ secret: 'mysecret', resave: false, saveUninitialized: false, store: store }));
app.use(csrfProtection);

app.use((req, res, next) => {
  if (!req.session.user) {
    return next();
  }
  User.findById(req.session.user._id)
    .then(user => {
      if (!user) {
        return next();
      }
      req.user = user;
    
      next();
    })
    .catch(err => {
      next(new Error(err));

    });
});
app.use((req, res, next) => {
  res.locals.isAuthenticated = req.session.isLoggedIn;
  res.locals.csrfToken = req.csrfToken();
  next();

});
app.use(express.static(path.join(__dirname, 'public')));

app.use(authRouter);
app.use(listRouter);
app.use(chatRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err);
  res.render('error');
});

module.exports = app;
