const {
  body
} = require('express-validator/check');

const User = require('../models/user');

exports.loginFormRequest = [
   body('email').isEmail().withMessage('Please enter a valid email.'),
     body('password', 'Your password is incorrect.').isLength({
       min: 5
     }).isAlphanumeric().trim()
];