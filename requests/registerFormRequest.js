const {
  body
} = require('express-validator/check');

const User = require('../models/user');

exports.registerFormRequest = [
  body('email').isEmail().withMessage('Please enter a valid email.').custom((value, {
    req
  }) => {
    return User.findOne({
      email: value
    }).then(userDoc => {
      if (userDoc) {
        return Promise.reject('The email already exists please pick another one.');
      }
    });
  }),
  body('password', 'Please enter a password with only numbers and text and at least 5 characters.').isLength({
    min: 5
  }).isAlphanumeric().trim(),
  body('confirm_password').trim().custom((value, {
    req
  }) => {
    if (value !== req.body.password) {
      throw new Error('Passwords have to match.');
    }
    return true;
  })
];

