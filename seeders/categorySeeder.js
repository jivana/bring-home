const Category = require('../models/category');

const mongoose= require('mongoose');
var slug = require('slug')

mongoose.connect('mongodb://ivana:nopass5@ds131765.mlab.com:31765/bring-home');
var categories=[
    new Category({
        name : 'Fruits & Vegetables',
        slug: slug('Fruits & Vegetables',{lower: true})
    }),
    new Category({
        name : 'Milk & Dairy products',
        slug: slug('Milk & Dairy products',{lower: true})
    }),
    new Category({
        name : 'Protein',
        slug: slug('Protein',{lower: true})
    }),
    new Category({
        name : 'Bread & Pastries',
        slug: slug('Bread & Pastries',{lower: true})
    }),
    new Category({
        name : 'Snacks & Sweets',
        slug: slug('Snacks & Sweets',{lower: true})
    }),
]
var done=0;
categories.forEach(element => {
        element.save(function(err,result){
            done++;
            if(done == categories.length){
        
               exit();
            }
        });
   
});

function exit(){
    mongoose.disconnect();
}