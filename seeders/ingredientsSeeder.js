const Ingredient=require('../models/ingredient');
const Category= require('../models/category');
const mongoose=require('mongoose');
mongoose.connect('mongodb://ivana:nopass5@ds131765.mlab.com:31765/bring-home');

var fruitsAndVegetables=[
    'Apple',
    'Watermelon',
    'Orange',
    'Pear',
    'Cherry',
    'Strawberry',
    'Nectarine',
    'Grape',
    'Mango',
    'Blueberry',
    'Pomegranate',
    'Plum',
    'Banana',
    'Raspberry',
    'Mandarin',
    'Jackfruit',
    'Papaya',
    'Kiwi',
    'Pineapple',
    'Lime',
    'Lemon',
    'Apricot',
    'Grapefruit',
    'Melon',
    'Coconut',
    'Avocado',
    'Peach',
    'acorn squash',
    'alfalfa sprout',
    'amaranth',
    'anise',
    'artichoke',
    'arugula',
    'asparagus',
    'aubergine',
    'azuki bean',
    'banana squash',
    'basil',
    'bean sprout',
    'beet',
    'black bean',
    'black-eyed pea',
    'bok choy',
    'borlotti bean',
    'broad beans',
    'broccoflower',
    'broccoli',
    'brussels sprout',
    'butternut squash',
    'cabbage',
    'calabrese',
    'caraway',
    'carrot',
    'cauliflower',
    'cayenne pepper',
    'celeriac',
    'celery',
    'chamomile',
    'chard',
    'chayote',
    'chickpea',
    'chives',
    'cilantro',
    'collard green',
    'corn',
    'corn salad',
    'courgette',
    'cucumber',
    'daikon',
    'delicata',
    'dill',
    'eggplant',
    'endive',
    'fennel',
    'fiddlehead',
    'frisee',
    'garlic',
    'gem squash',
    'ginger',
    'green bean',
    'green pepper',
    'habanero',
    'herbs and spice',
    'horseradish',
    'hubbard squash',
    'jalapeno',
    'jerusalem artichoke',
    'jicama',
    'kale',
    'kidney bean',
    'kohlrabi',
    'lavender',
    'leek ',
    'legume',
    'lemon grass',
    'lentils',
    'lettuce',
    'lima bean',
    'mamey',
    'mangetout',
    'marjoram',
    'mung bean',
    'mushroom',
    'mustard green',
    'navy bean',
    'new zealand spinach',
    'nopale',
    'okra',
    'onion',
    'oregano',
    'paprika',
    'parsley',
    'parsnip',
    'patty pan',
    'pea',
    'pinto bean',
    'potato',
    'pumpkin',
    'radicchio',
    'radish',
    'rhubarb',
    'rosemary',
    'runner bean',
    'rutabaga',
    'sage',
    'scallion',
    'shallot',
    'skirret',
    'snap pea',
    'soy bean',
    'spaghetti squash',
    'spinach',
    'squash ',
    'sweet potato',
    'tabasco pepper',
    'taro',
    'tat soi',
    'thyme',
    'topinambur',
    'tubers',
    'turnip',
    'wasabi',
    'water chestnut',
    'watercress',
    'white radish',
    'yam',
    'zucchini'
];

var milkAndDairyProducts=[
    'Butter',
    'Cheddar',
    'Cheese',
    'Cream',
    'Eggs',
    'Feta',
    'Margarine',
    'Milk',
    'Mozzarella',
    'Parmesan',
    'Yogurt',
    'Sour cream'
];

var proteins=[
    'beef',
    'chicken',
    'turkey',
    'pork',
    'Partridge',
    'Mutton Liver',
    'Ham',
    'Kidney Meat',
    'Crab',
    'Chicken Liver',
    'Chops',
    'Lamb Meat',
    'Bacon',
    'Shrimp',
    'Tuna Fish',
    'Shellfish',
    'Sardines',
    'Salmon',
    'Prawns',
    'Mussels',
    'Fish',
    'mushrooms'
];

var breadAndPastries=[
    'Bagels',
    'Baguette', 
    'Bread', 
    'Buns',
    'Crispbread',
     'Croissant',
    'Donuts', 
    'Muffins',
    'Pancakes mix',
    'Pie',
    'Pizza dough',
    'Puff pastry', 
    'Pumpkin Pie', 
    'Rolls',  
    'Scones', 
    'Sliced bread',  
    'Toast', 
    'Tortillas', 
    'Waffles',

];

var snacksAndSweets=[
    'Cake',
    'Candy ',
    'Cereal bar' ,
    'Chewing gum ',
    'Chips',
    'Chocolate',
    'Cookies',
    'Crackers',
    'Dried fruit',
    'Gingerbread ',
    'Honey ',
    'Jam',
    'Marshmallows',
    'Nougat cream ',
    'Peanuts ',
    'Pop corn',
    'Pretzels',
    'Snacks',
    'Tortilla Chips',
];


 async function run ()  {
    
    var result = await Category.find({});
    
    result.forEach(async element => {
            var slug= await element.slug;
            if(slug =='fruits-and-vegetables'){
                fruitsAndVegetables.forEach(async (data) => {
                    var ing= new Ingredient({
                        'name' :  data,
                        'category':  element._id
                    })
              
                    await ing.save();
                  
                });
                
            }
          
            if(slug =='milk-and-dairy-products'){
                milkAndDairyProducts.forEach(async (data) => {
                    var ing= new Ingredient({
                        'name' : data,
                        'category': element._id
                    })
                    await ing.save();
                });
            }
            if(slug =='protein'){
                proteins.forEach(async (data) => {
                    var ing= new Ingredient({
                        'name' : data,
                        'category': element._id
                    })
                   await ing.save();
                });
            }
            if(slug =='snacks-and-sweets'){
                snacksAndSweets.forEach( async (data) => {
                    var ing= new Ingredient({
                        'name' : data,
                        'category': element._id
                    })
                   await ing.save();
                });
            }
            if(slug =='bread-and-pastries'){
                breadAndPastries.forEach(async (data) => {
                    var ing= new Ingredient({
                        'name' : data,
                        'category': element._id
                    })
                   await ing.save();
                });
            }
          
          
    });
   
   
   
};
run();



 



