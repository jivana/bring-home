var express = require('express');
var router = express.Router();
const authController = require('../controllers/authController.js');
const {
  registerFormRequest
} = require('../requests/registerFormRequest');
const isAuth = require('../middleware/is_auth');
const refirectIfAuth = require('../middleware/redirectIfAuth');
const { loginFormRequest } = require('../requests/loginFromRequest');

/* GET register */
router.get('/login', refirectIfAuth,authController.getLogin);
router.post('/login', loginFormRequest, authController.postLogin);
router.get('/logout', authController.getLogout);
router.get('/register',refirectIfAuth, authController.getRegister);
router.post('/register', registerFormRequest, authController.postRegister);
router.get('/settings', isAuth, authController.getSettings);
router.post('/settings', isAuth,authController.postSettings);
router.get("/reset", refirectIfAuth, authController.getReset);
router.post("/reset", authController.postReset);
router.get("/reset-password/:token", authController.getResetPassword);
router.post("/reset-password", authController.postResetPassword);


module.exports = router;