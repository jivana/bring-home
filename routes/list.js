var express = require('express');
var router = express.Router();

const listController = require('../controllers/listController');
const isAuth = require('../middleware/is_auth');
const {
  body
} = require('express-validator/check');
const {
  registerFormRequest
} = require('../requests/registerFormRequest');

router.get('/', isAuth, listController.getLists);
router.get('/add-list', isAuth, listController.addList);
router.post('/add-list', [
  body('name').not().isEmpty()
  .trim().withMessage('The name is required.')
], isAuth, listController.postList);
router.get('/edit-list/:listId', isAuth, listController.editList);
router.post('/edit-list/:listId', isAuth, listController.postEditList);
router.get('/all-lists', isAuth, listController.getAllLists);
router.get('/add-members/:listId', isAuth, listController.getAddMembers);
router.post('/add-members/:listId', isAuth, listController.addMembers);
router.get('/invitation/:listId/:email', listController.invitation);
router.post('/invitation', registerFormRequest, listController.postInvitation);
router.get('/delete-member/:userId/:listId', isAuth, listController.deleteMember);
router.get('/delete-list/:listId', isAuth, listController.deleteList);
router.post('/add-ing/:listId', isAuth, listController.addIng);

module.exports = router;
