var express = require('express');
var router = express.Router();

const chatController = require('../controllers/chatController');
const isAuth = require('../middleware/is_auth');
const {
  body
} = require('express-validator/check');

router.get('/chat/:listId', isAuth, chatController.getChat);
router.post('/chat/:listId', isAuth, chatController.postMessages);



module.exports = router;
