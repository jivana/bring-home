$(document).ready(function () {

    $('#bhListsTable').DataTable({
        "paging": true,
        "pageLength": 10,
        "ordering": false,
        "info": false,
        "lengthChange": false
      });
});