$(document).ready(function() {
  var mydiv = $(".bh-msg-card-body");
  mydiv.scrollTop(mydiv.prop("scrollHeight"));

  calc_min_height();
  $('[data-toggle="tooltip"]').tooltip();

  // Toggle Side panel
  $("#side_panel_toggle").on("click", function() {
    $("#side_panel:not(.show)").addClass("show");
  });

  $("#side_panel .do-close").on("click", function() {
    $("#side_panel.show").removeClass("show");
  });

  //Select Picker
  $(".selectpicker").selectpicker();

  if ($("#content-slider").length) {
    $("#content-slider").lightSlider({
      loop: true,
      keyPress: true,
      pager: false,
      prevHtml: '<i class="fa fa-chevron-left"></i>',
      nextHtml: '<i class="fa fa-chevron-right"></i>',
      responsive: [
        {
          breakpoint: 990,
          settings: {
            item: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            item: 1
          }
        }
      ]
    });
  }
});

$(window).resize(function() {
  calc_min_height();
});

function calc_min_height() {
  $("main, .do-login-wrap.full").css("min-height", window.innerHeight);
}
