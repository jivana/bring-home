const User=require('../models/user');
const {
  validationResult
}=require('express-validator/check');
const bcrypt=require('bcryptjs');
const crypto=require('crypto');
const nodemailer=require("nodemailer");
const sendGridTransport=require("nodemailer-sendgrid-transport");
const transporter=nodemailer.createTransport(
  sendGridTransport({
    auth: {
      api_key:
        "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"
    }
  })
);

exports.getLogin=(req, res, next) => {
  message=null;
  res.status(200).render('auth/login', {
    pageTitle: 'Bring Home',
    errorMessage: message,
    oldInput: {
      email: "",
    },
    messages: req.flash("info"),

  });
};

exports.postLogin=async (req, res, next) => {
  try {
    const email=req.body.email;
    const password=req.body.password;
    const errors=validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).render('auth/login', {
        pageTitle: 'Login',
        errorMessage: errors.array()[0].msg,
        oldInput: {
          email: email,
        },
        messages: []

      });
    }
    const user=await User.findOne({
      email: email
    });
    if (!user) {
      return res.status(422).render('auth/login', {
        pageTitle: 'Login',
        errorMessage: 'Invalid email or password!',
        oldInput: {
          email: email,
          password: password,

        },
        validationErrors: []
      });
    }
    const doMatch=await bcrypt.compare(password, user.password);

    if (!doMatch) {
      return res.status(422).render('auth/login', {
        pageTitle: 'Login',
        errorMessage: 'Invalid email or password!',
        oldInput: {
          email: email,
        },
        validationErrors: [],
        messages:[]
      });
    }
    else {
      //save user to session
      req.session.user=user;
      req.session.isLoggedIn=true;
      // return req.session.save;
      res.redirect('/');
    }

  } catch (err) {
    console.log(err);
  }

};
exports.getLogout=(req, res, next) => {
  if (req.session) {
    req.session.destroy(err => {
      if (err) {
        console.log(err);
      }
      else {
        return res.redirect('/');
      }
    })
  }
}

exports.getRegister=(req, res, next) => {

  message=null;
  res.status(200).render('auth/register', {
    pageTitle: 'Register',
    errorMessage: message,
    oldInput: {
      email: "",
    }
  });
};

exports.postRegister=async (req, res, next) => {
  try {
    const email=req.body.email;
    const password=req.body.password;
    const errors=validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).render('auth/register', {
        pageTitle: 'Register',
        errorMessage: errors.array()[0].msg,
        oldInput: {
          email: email,
        },

      });
    }
    const hashedPass=await bcrypt.hash(password, 12);
    const user=new User({
      email: email,
      password: hashedPass
    });
    const result=await user.save();
    return res.redirect('/');
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode=500;
    }
    next(err);
  }


};

exports.getSettings=(req, res, next) => {
  res.status(200).render('settings', {
    pageTitle: 'Settings',
    user: req.user
  });
}

exports.postSettings=async (req, res, next) => {
  try {
    const first_name=req.body.first_name;
    const last_name=req.body.last_name;
    const avatar=req.body.avatar;
    const user=await User.findById(req.session.user._id);
    user.first_name=first_name;
    user.last_name=last_name;
    user.avatar=avatar;
    await user.save();

    return res.redirect('/');
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode=500;
    }
    next(err);
  }

}

exports.getReset=(req, res, next) => {
  const errorMessage=null;
  res.status(200).render("auth/reset", {
    pageTitle: "Bring Home | Reset",
    errorMessage: errorMessage,
    oldInput: {
      email: "",
    }
  });
}

exports.postReset=async (req, res, next) => {
  try {

    const buffer=crypto.randomBytes(32);
    const token=buffer.toString('hex');

    const user=await User.findOne({email: req.body.email});
    if (user==null) {
      console.log('noo user');
      return res.status(422).render('auth/reset', {
        pageTitle: 'Reset',
        errorMessage: 'This email does not exist in our system.',
        oldInput: {
          email: req.body.email,
        }
      });
    }
    else {
      user.resetToken=token;
      const redirectUrl="http://localhost:3000/reset-password/"+token;
      user.resetTokenExpiration=Date.now()+3600000;
      const updatedUser=await user.save();
      transporter.sendMail({
        to: req.body.email,
        from: "noreply@bring-home.com",
        fromname: "BringHome",
        subject: "Password Reset",
        html:
          "<div>You requested a password reset. "+
          "</div>"+
          "<div>Click this link to set a new password.</div>"+
          '<a href="'+
          redirectUrl+
          '" style="background-color: #4CAF50;border: none;color: white; padding: 15px 32px;text-align: center; text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer; ">Reset</a>'
      });
      return res.redirect('/');
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode=500;
    }
    next(err);
  }

}

exports.getResetPassword=async (req, res, next) => {

  const token=req.params.token;
  var expired=0;
  var userID='';
  const user=await User.findOne({resetToken: token, resetTokenExpiration: {$gt: Date.now()}});
  if (user==null) {
    expired=1;
  }
  else {
    userID=user._id;
  }


  return res.render('auth/reset-password', {
    pageTitle: 'Bring Home | Reset Password',
    path: 'reset',
    expired: expired,
    messages: req.flash("info"),
    userID: userID,
    passToken: token
  })
}

exports.postResetPassword=async (req, res, next) => {

  try {
    const newPass=req.body.password;
    const userID=req.body.userId;
    const passToken=req.body.passToken;
    var messages = null;
    const user= await User.findOne({resetToken: passToken, resetTokenExpiration :{$gt : Date.now() },
    _id: userID
  });
  if(user != null){
    if(newPass.length < 5){
      message = 'The password must be at least 5 characters.'
      backURL = req.header("Referer") || "/";
      message = {
        type: "error",
        msg: message
      };
      req.flash("info", message);

      return res.redirect(backURL);
    }
    else{
      const hashedPass= await bcrypt.hash(newPass,12);
      user.password= hashedPass;
      user.resetToken= null;
      user.resetTokenExpiration=null;
      await user.save();
      message = 'You have successfully updated your password.'
      message = {
        type: "success",
        msg: message
      };
      req.flash("info", message);

      return res.redirect('/');
    }
    
  }
  else{
    message = 'Ops. Something went wrong.'
    backURL = req.header("Referer") || "/";
      message = {
        type: "error",
        msg: message
      };
      req.flash("info", message);

      return res.redirect(backURL);
  }

  } catch (err) {
    if (!err.statusCode) {
      err.statusCode=500;
    }
    next(err);
  }
}