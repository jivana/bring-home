const { validationResult } = require("express-validator/check");
const nodemailer = require("nodemailer");
const sendGridTransport = require("nodemailer-sendgrid-transport");
const List = require("../models/list");
const User = require("../models/user");
const Ingredient= require("../models/ingredient");
const io = require("../socket");
var ObjectId = require("mongoose").Types.ObjectId;
const Chat = require("../models/chat");
const Message = require("../models/message");


const transporter = nodemailer.createTransport(
  sendGridTransport({
    auth: {
      api_key:
        "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"
    }
  })
);
const bcrypt = require("bcryptjs");

exports.getLists = (req, res, next) => {
  console.log(req.flash('infoo'));
  res.status(200).render("lists/lists", {
    pageTitle: "Bring Home | Lists",
    user: req.user,
    messages: req.flash("info")
  });
};

exports.addList = (req, res, next) => {
  res.status(200).render("lists/add-list", {
    pageTitle: "Bring Home | Add List",
    user: req.user,
    oldInput: {
      name: ""
    },
    messages: req.flash("info")
  });
};

exports.postList = async (req, res, next) => {
  try {
    const name = req.body.name;
    const errors = validationResult(req);
    let message;

    if (!errors.isEmpty()) {
      messageObj = {
        type: "error",
        msg: errors.array()[0].msg
      };
      req.flash("info", messageObj);
      message = req.flash("info");
      return res.status(422).render("lists/add-list", {
        pageTitle: "Bring Home | Add List",
        oldInput: {
          name: name
        },
        messages: message,
        user: req.user
      });
    }

    const list = new List({
      name: name,
      members: [req.user._id],
      creator: req.user._id
    });

    await list.save();

    message = {
      type: "success",
      msg: "You have successfully created the list."
    };
    req.flash("info", message);
    return res.redirect("/");
  } catch (err) {
    console.log(err);
  }
};

exports.getAllLists = async (req, res, next) => {
  const lists = await List.find({})
    .where("members")
    .in([req.user._id]);
  res.status(200).render("lists/all-lists", {
    pageTitle: "Bring Home | Lists",
    user: req.user,
    messages: req.flash("info"),
    lists: lists,
    authUser: req.user._id
  });
};

exports.getAddMembers = (req, res, next) => {
  listId = req.params.listId;
  res.status(200).render("lists/add-members", {
    pageTitle: "Bring Home | Add Members",
    user: req.user,
    messages: req.flash("info"),
    listId: listId
  });
};

exports.addMembers = async (req, res, next) => {
  const listId = req.params.listId;
  const user = await User.findById(req.user._id);
  const list = await List.findById(listId);
  const creatorEmail = user.email;
  const emails = [];
  for (var key in req.body) {
    if (key != "_csrf") {
      let value = req.body[key];
      emails.push(value);
    }
  }
  //TODO Send link with list id and new registration link

  const redirectUrl = "http://localhost:3000/invitation/" + listId;

  //Send emails
  emails.forEach(function(email) {
    transporter.sendMail({
      to: email,
      from: "noreply@bring-home.com",
      fromname: "BringHome",
      subject: "Bring Home Invitation",
      html:
        "<div>You have been invited to Bring Home - Shopping List by " +
        creatorEmail +
        "</div>" +
        "<div>To accept the invitation for <strong>" +
        list.name +
        "</strong> click on the button below.</div>" +
        '<a href="' +
        redirectUrl +
        "/" +
        email +
        '" style="background-color: #4CAF50;border: none;color: white; padding: 15px 32px;text-align: center; text-decoration: none;display: inline-block;font-size: 16px;margin: 4px 2px;cursor: pointer; ">Accept Invitation</a>'
    });
  });

  message = {
    type: "success",
    msg: "Your invitations have been successfully send."
  };
  req.flash("info", message);
  return res.redirect("/");
};

exports.invitation = async (req, res, next) => {
  const listId = req.params.listId;
  const email = req.params.email;
  const userExist = await User.findOne({ email: email });
  const list = await List.findById(listId);
  let num;
  if (userExist == null) {
    num = 0;
  } else {
    num = 1;
    const res=await List.findById(listId)
    .where("members")
    .in([userExist._id]);
    if(res == null){
      list.members.push(userExist._id);
      await list.save();
    }
   
  }

  const listOwner = await User.findById(list.creator._id);
  res.status(200).render("lists/invitation", {
    pageTitle: "Bring Home | Invitation",
    list: list,
    listOwner: listOwner,
    email: email,
    messages: req.flash("info"),
    listId: listId,
    userExist: num
  });
};

exports.postInvitation = async (req, res, next) => {
  try {
    const email = req.body.email;
    const password = req.body.password;
    const listId = req.body.listId;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      backURL = req.header("Referer") || "/";
      message = {
        type: "error",
        msg: errors.array()[0].msg
      };
      req.flash("info", message);

      return res.redirect(backURL);
    }

    const hashedPass = await bcrypt.hash(password, 12);
    const user = new User({
      email: email,
      password: hashedPass
    });
    const savedUser = await user.save();
    const list = await List.findById(listId);
    list.members.push(savedUser._id);
    await list.save();
    return res.redirect("/");
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.editList = async (req, res, next) => {
  const listId = req.params.listId;
  const list = await List.findById(listId).populate("members");
  res.status(200).render("lists/edit-list", {
    pageTitle: "Bring Home | Edit List",
    user: req.user,
    list: list,
    messages: req.flash("info")
  });
};

exports.postEditList = async (req, res, next) => {
  try {
    const listId = req.params.listId;
    const list = await List.findById(listId);
    const listName = req.body.name;

    if (listName != "") {
      list.name = listName;
      await list.save();
      message = {
        type: "success",
        msg: "You have successfully edited the list."
      };
      req.flash("info", message);
      return res.redirect("/");
    } else {
      message = {
        type: "error",
        msg: "The name is required."
      };
      req.flash("info", message);
      backURL = req.header("Referer") || "/";
      return res.redirect(backURL);
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.deleteMember = async (req, res, next) => {
  try {
    const memberId = req.params.userId;
    const listId = req.params.listId;

    await List.update(
      {
        _id: listId
      },
      {
        $pull: {
          members: memberId
        }
      }
    );
    message = {
      type: "success",
      msg: "You have successfully deleted the member."
    };
    req.flash("info", message);
    backURL = req.header("Referer") || "/";
    return res.redirect(backURL);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.deleteList = async (req, res, next) => {
  try {
    const listId = req.params.listId;
    await List.deleteOne({
      _id: listId
    });

    message = {
      type: "success",
      msg: "You have successfully deleted the list.."
    };
    req.flash("info", message);
    backURL = req.header("Referer") || "/";
    return res.redirect(backURL);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.addIng = async (req, res, next) => {
  try {
    const listId = req.params.listId;
    const ingId = req.body.ingId; 
    const ingName = req.body.ingName;
    const list = await List.findById(listId);
    const ing= await Ingredient.findById(ingId);
    if(ingId != undefined){
      const ingExist=  await List.findOne({
        ingredients: new ObjectId(ingId),
        _id: listId
      });

     if(ingExist != null){
        await List.update(
          {
            _id: listId
          },
          {
            $pull: {
              ingredients: ingId
            }
          }
        );
        io.getIO().emit("ing", {
          action: "remove",
          ing: ingId,
        });
        sendMsgBought(listId,req.user,ing.name);
     }
     else{
      list.ingredients.push(ingId);
      await list.save(); 
      console.log('add ing');
      io.getIO().emit("ing", {
        action: "add",
        ing: ing,
      });
     }
    }
    if(ingName != undefined){
      const ingExist=  await List.findOne({
        _id: listId
      },
      {custom_ings: {$elemMatch: {ingName}}}
      );

      console.log(ingExist);
     if(ingExist != null){
        await List.update(
          {
            _id: listId
          },
          {
            $pull: {
              custom_ings: ingName
            }
          }
        );
        io.getIO().emit("ing", {
          action: "remove",
          ing: ingName,
        });
     }
     sendMsgBought(listId,req.user,ingName);
    }
    
    return res.status(201).json({
      message: "success"
    });
  } catch (err) {
  
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

async function sendMsgBought(listId,user,ing){
  const chatExist = await Chat.findOne({
    list_id: new ObjectId(listId)
  }).populate("messages");
  if (chatExist != null) {
    message = new Message({
      author: user._id,
      body: "bought " + ing
    });
    await message.save();
    chatExist.messages.push(message._id);
    await chatExist.save();
  } else {
    message = new Message({
      author: user._id,
      body: "bought " + ing
    });
    await message.save();
    const chat = new Chat({
      list_id: new ObjectId(listId),
      messages: [message._id]
    });

    await chat.save();
  }
  io.getIO().emit("message", {
    action: "create",
    msg: message,
    user: user
  });
}