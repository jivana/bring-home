var ObjectId = require("mongoose").Types.ObjectId;
const List = require("../models/list");
const Chat = require("../models/chat");
const Message = require("../models/message");
const Ingridient= require('../models/ingredient');
const Category= require('../models/category');
const User = require("../models/user");
const io = require("../socket");

exports.getChat = async (req, res, next) => {
  const lists = await List.find({})
    .where("members")
    .in([req.user._id]);
  const list = await List.findById(req.params.listId).populate("ingredients");
  var chatExist = [];
  chatExist = await Chat.findOne({
    list_id: new ObjectId(req.params.listId)
  }).populate("messages");
  var newData = [];
  var aggregateQuery = [
    {"$group" : {_id:"$category",
    data : { $addToSet: { id: '$_id', name: '$name' }}
  }}
  ];
  const ingridients = await Ingridient.aggregate(aggregateQuery).exec();
  const ingData=[];
  for (var j = 0, len = ingridients.length; j < len; j++) {
    const cat = await Category.findById(ingridients[j]._id);
    const catName= cat.name;
    const data=ingridients[j].data;
    const ingObj = { catName , data};
    ingData[j] = ingObj;
  }
 
  if (chatExist != null) {
    const msgArr = chatExist.messages;
  
    for (var i = 0, len = msgArr.length; i < len; i++) {
      const userObj = await User.findOne({ _id: msgArr[i].author });
      const msgObj = msgArr[i];
      const chatObj = { userObj, msgObj };
      newData[i] = chatObj;
    }
  }
  

  res.status(200).render("chat/chat", {
    pageTitle: "Bring Home | Lists",
    user: req.user,
    lists: lists,
    list: list,
    messages: newData,
    ings: ingData
  });
};

exports.postMessages = async (req, res, next) => {
  const messageBody = req.body.msg;
  const listId = req.params.listId;
  let message;
  const chatExist = await Chat.findOne({
    list_id: new ObjectId(listId)
  }).populate("messages");
  const list = await List.findById(listId);
  if(messageBody.startsWith('buy',0)){
    var ing = messageBody.substring(4);
   
    list.custom_ings.push(ing.toLowerCase().trim());
    await list.save();
    io.getIO().emit("ing", {
      action: "add-custom",
      ing: ing.toLowerCase().trim(),
    });
   
  }
  if(messageBody.startsWith('bought',0)){
   
    var ing = messageBody.substring(7).trim();
    const ingExist=  await List.findOne({
      custom_ings: ing.toLowerCase().trim(),
      _id: listId
    });
    if(ingExist != null){
    await List.updateOne(
          {
            _id: listId
          },
          {
            $pull: {
              custom_ings: ing.toLowerCase().trim()
            }
          }
        );
        io.getIO().emit("ing", {
          action: "remove",
          ing: ing.toLowerCase().trim(),
        });
    } 
   
  }
  if (chatExist != null) {
    message = new Message({
      author: req.user._id,
      body: messageBody
    });
    await message.save();
    chatExist.messages.push(message._id);
    await chatExist.save();
  } else {
    message = new Message({
      author: req.user._id,
      body: messageBody
    });
    await message.save();
    const chat = new Chat({
      list_id: new ObjectId(listId),
      messages: [message._id]
    });

    await chat.save();
  }
  io.getIO().emit("message", {
    action: "create",
    msg: message,
    user: req.user
  });
  res.status(201).json({
    message: "success"
  });
};
