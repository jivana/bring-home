const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  resetToken: String,
  resetTokenExpiration: Date,
  first_name: {
     type: String,
  },
  last_name: {
    type: String,
  },
  avatar: {
    type: String
  }
});

module.exports = mongoose.model('User', userSchema);