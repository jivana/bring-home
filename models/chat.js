const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const chatSchema = new Schema(
  {
    list_id: {
      type: Schema.Types.ObjectId,
      ref: "List"
    },
    messages: [
      {
        type: Schema.Types.ObjectId,
        ref: "Message"
      }
    ]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Chat", chatSchema);
