const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const listSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  members: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  ingredients: [{
    type: Schema.Types.ObjectId,
    ref: 'Ingredient'
  }],
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  custom_ings:[]

}, {
  timestamps: true
});

module.exports = mongoose.model('List', listSchema);